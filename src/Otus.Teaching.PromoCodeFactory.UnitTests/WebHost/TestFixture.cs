using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.BusinessLogic.Partner;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost;

public class TestFixture
{
    public IServiceProvider ServiceProvider { get; set; }
    public IServiceCollection ServiceCollection { get; set; }

    public TestFixture()
    {
        var builder = new ConfigurationBuilder();
        var configuration = builder.Build();

        IServiceCollection services = new ServiceCollection();
        services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
        services.AddScoped<IPartnerLogic, PartnerLogic>();
        services.AddScoped<PartnersController>();

        ServiceCollection = Configuration.GetServiceCollection(configuration, "Tests");
        var serviceProvider = GetServiceProvider();
        ServiceProvider = serviceProvider;
    }

    private IServiceProvider GetServiceProvider()
    {
        var serviceProvider = ServiceCollection
            .BuildServiceProvider();
        return serviceProvider;
    }

    public void Dispose()
    {
    }
}