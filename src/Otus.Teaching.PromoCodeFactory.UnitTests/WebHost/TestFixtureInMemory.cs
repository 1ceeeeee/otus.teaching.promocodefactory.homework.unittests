using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public class TestFixtureInMemory : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }
        public IServiceCollection ServiceCollection { get; set; }
        
        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixtureInMemory()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            
            IServiceCollection services = new ServiceCollection();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddDbContext<DataContext>(x =>
            {
                x.UseInMemoryDatabase("database");
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            services.AddScoped<PartnersController>();
            
            ServiceCollection = Configuration.GetServiceCollection(configuration, "Tests");
            var serviceProvider = GetServiceProvider();
            ServiceProvider = serviceProvider;
        }

        private IServiceProvider GetServiceProvider()
        {
            var serviceProvider = ServiceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();
            return serviceProvider;
        }

        public void Dispose()
        {
        }
    }
    
}