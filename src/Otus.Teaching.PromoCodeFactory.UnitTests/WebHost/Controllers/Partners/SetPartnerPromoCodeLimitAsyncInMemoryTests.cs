using System;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Partner;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Shouldly;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class SetPartnerPromoCodeLimitAsyncInMemoryTests : IClassFixture<TestFixtureInMemory>
{
    private readonly IFixture _autoFixture;
    private readonly IRepository<Partner> _partnerRepository;
    private readonly PartnersController _partnersController;

    public SetPartnerPromoCodeLimitAsyncInMemoryTests(TestFixtureInMemory testFixtureInMemory)
    {
        var serviceProvider = testFixtureInMemory.ServiceProvider;
        _partnerRepository = serviceProvider.GetService<IRepository<Partner>>();
        _partnersController = serviceProvider.GetService<PartnersController>();
        _autoFixture = new Fixture();
        _autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

        var seeder = serviceProvider.GetService<IDbInitializer>();
        seeder.InitializeDb();
    }
    
    [Fact]
    public async Task SetPartnerPromoCodeLimitAsync_CorrectLimits_SavedToDb()
    {
        // Arrange 
        var partnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

        var request = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
            .With(x => x.Limit, 5)
            .Create();

        // Act
        (await _partnersController.SetPartnerPromoCodeLimitAsync(
            partnerId,
            request)).ShouldBeOfType<CreatedAtActionResult>();

        // Assert
        var partnerInDb = await _partnerRepository.GetByIdAsync(partnerId);
        partnerInDb.PartnerLimits.Should().ContainSingle(x => x.Limit == request.Limit);
    }
}