﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Constants;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Partner;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Shouldly;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests 
    {
        private readonly IFixture _autoFixture;
        private readonly PartnersController _controller;
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock
            = new Mock<IRepository<Partner>>();

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _controller = new PartnersController(_partnerRepositoryMock.Object);
            _autoFixture = new Fixture();
            _autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IfPartnerIdNotFound_Returns404()
        {
            //Arrange
            var partnerId = _autoFixture.Create<Guid>();

            //Act
            var result = await _controller.SetPartnerPromoCodeLimitAsync(
                partnerId,
                new SetPartnerPromoCodeLimitRequest());

            //Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotActive_Returns400()
        {
            //Arrange
            var partner = _autoFixture.Build<Partner>()
                .With(p => p.IsActive, false)
                .Create();

            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = (await _controller.SetPartnerPromoCodeLimitAsync(
                partner.Id,
                new SetPartnerPromoCodeLimitRequest())).ShouldBeOfType<BadRequestObjectResult>();

            //Assert
            result.Value.ShouldBe(PartnerMessages.PartnerIsNotActive);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerHasLimits_ShouldResetNumberIssuedPromoCodesToZero()
        {
            // Arrange 
            var partnerLimit = _autoFixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.CancelDate)
                .Create();
            var partner = _autoFixture.Build<Partner>()
                .With(x => x.PartnerLimits,
                    new List<PartnerPromoCodeLimit>() { partnerLimit })
                .With(p => p.NumberIssuedPromoCodes, 3)
                .With(p => p.IsActive, true)
                .Create();

            var request = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(p => p.Limit, 5)
                .Create();

            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            (await _controller.SetPartnerPromoCodeLimitAsync(
                partner.Id,
                request)).ShouldBeOfType<CreatedAtActionResult>();

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async Task
            SetPartnerPromoCodeLimitAsync_PartnerDoesNotHasActiveLimits_ShouldNotResetNumberIssuedPromoCodesToZero()
        {
            // Arrange 
            var partnerLimit = _autoFixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.CancelDate, DateTime.Now.AddDays(-1))
                .Create();
            var numberIssuedPromoCodes = 3;
            var partner = _autoFixture.Build<Partner>()
                .With(x => x.PartnerLimits, new List<PartnerPromoCodeLimit>() { partnerLimit })
                .With(p => p.NumberIssuedPromoCodes, numberIssuedPromoCodes)
                .With(p => p.IsActive, true)
                .Create();

            var request = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(p => p.Limit, 5)
                .Create();

            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            (await _controller.SetPartnerPromoCodeLimitAsync(
                partner.Id,
                request)).ShouldBeOfType<CreatedAtActionResult>();

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(numberIssuedPromoCodes);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerHasLimits_ShouldDisablePreviousLimit()
        {
            // Arrange 
            var partnerLimit = _autoFixture.Build<PartnerPromoCodeLimit>()
                .Without(x => x.CancelDate)
                .Create();
            var partner = _autoFixture.Build<Partner>()
                .With(x => x.PartnerLimits,
                    new List<PartnerPromoCodeLimit>() { partnerLimit })
                .With(x => x.IsActive, true)
                .Create();

            var request = _autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 5)
                .Create();

            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            (await _controller.SetPartnerPromoCodeLimitAsync(
                partner.Id,
                request)).ShouldBeOfType<CreatedAtActionResult>();

            // Assert
            partner.PartnerLimits.Should().ContainSingle(x => x.CancelDate.HasValue);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PassZeroLimit_Returns400()
        {
            //Arrange
            var partner = _autoFixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .Create();

            _partnerRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            //Act
            var result = (await _controller.SetPartnerPromoCodeLimitAsync(
                    partner.Id,
                    new SetPartnerPromoCodeLimitRequest { Limit = 0 }))
                .ShouldBeOfType<BadRequestObjectResult>();

            //Assert
            result.Value.ShouldBe(PartnerMessages.LimitMustBeMoreThanZero);
        }
        
    }
}