using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.BusinessLogic.Partner;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

namespace Otus.Teaching.PromoCodeFactory.UnitTests;

public static class Configuration
{
    public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
    {
        var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();
        services.AddDbContext<DataContext>(options =>
        {
            options.UseInMemoryDatabase("InMemoryDb", builder => { });
            options.UseInternalServiceProvider(serviceProvider);
        });
        services.AddTransient<DbContext, DataContext>();
        return services;
    }
    
    public static IServiceCollection GetServiceCollection(
        IConfigurationRoot configuration, 
        string serviceName, 
        IServiceCollection serviceCollection = null)
    {
        if (serviceCollection == null)
        {
            serviceCollection = new ServiceCollection();
        }

        serviceCollection
            .AddSingleton(configuration)
            .AddSingleton((IConfiguration)configuration)
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
            .AddScoped<IPartnerLogic, PartnerLogic>()
            .AddScoped<IDbInitializer, EfDbInitializer>()
            .AddScoped<PartnersController>();

        return serviceCollection;
    }
    
}