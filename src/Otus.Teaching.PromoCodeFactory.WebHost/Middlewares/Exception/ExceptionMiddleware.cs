using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Otus.Teaching.PromoCodeFactory.BusinessLogic.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Middlewares.Exception;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;

    public ExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (EntityNotFound ex)
        {
            await HandleExceptionAsync(httpContext, ex);
        }
        catch (System.Exception ex)
        {
            await HandleExceptionAsync(httpContext, ex);
        }
    }

    private async Task HandleExceptionAsync(
        HttpContext context,
        System.Exception exception)
    {
        context.Response.ContentType = "application/json";

        var statusCode = exception switch
        {
            EntityNotFound => (int)HttpStatusCode.NotFound,
            EntityBadQuery => (int)HttpStatusCode.BadRequest,
            _ => (int)HttpStatusCode.InternalServerError
        };
        await context.Response.WriteAsync(
            new ErrorDetails()
            {
                StatusCode = statusCode,
                Message = exception.Message
            }.ToString() ?? string.Empty);
    }
}