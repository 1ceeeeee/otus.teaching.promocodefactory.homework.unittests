﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.RoleItem
{
    public class RoleItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
    }
}