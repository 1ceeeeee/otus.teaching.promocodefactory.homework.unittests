﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Preference
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}