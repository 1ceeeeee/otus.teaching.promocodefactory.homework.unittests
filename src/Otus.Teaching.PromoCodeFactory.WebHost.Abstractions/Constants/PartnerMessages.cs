namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Constants;

public static class PartnerMessages
{
    public static readonly string PartnerIsNotActive = "Данный партнер не активен";
    public static readonly string LimitMustBeMoreThanZero = "Лимит должен быть больше 0";
}