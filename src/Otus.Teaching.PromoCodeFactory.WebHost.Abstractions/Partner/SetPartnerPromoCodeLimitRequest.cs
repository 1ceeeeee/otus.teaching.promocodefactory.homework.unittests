﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Partner
{
    public class SetPartnerPromoCodeLimitRequest
    {
        public DateTime EndDate { get; set; }
        public int Limit { get; set; }
    }
}