﻿using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Preference;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.PromoCode;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Customer
{
    public class CustomerResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponse> Preferences { get; set; }
        public List<PromoCodeShortResponse> PromoCodes { get; set; }

        public CustomerResponse()
        {
            
        }
    }
}