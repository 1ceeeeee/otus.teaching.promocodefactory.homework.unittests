using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Partner;

namespace Otus.Teaching.PromoCodeFactory.BusinessLogic.Partner;

public interface IPartnerLogic
{
    Task<IEnumerable<PartnerResponse>> GetPartnersAsync();

    Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId);

    Task<(Guid PartnerId, Guid NewLimitId)> SetPartnerPromoCodeLimitAsync(
        Guid id,
        SetPartnerPromoCodeLimitRequest request);

    Task CancelPartnerPromoCodeLimitAsync(Guid id);
}