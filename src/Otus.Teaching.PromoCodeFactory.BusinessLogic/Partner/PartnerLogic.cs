using Otus.Teaching.PromoCodeFactory.BusinessLogic.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Abstractions.Partner;

namespace Otus.Teaching.PromoCodeFactory.BusinessLogic.Partner;

public class PartnerLogic : IPartnerLogic
{
    private readonly IRepository<Core.Domain.PromoCodeManagement.Partner> _partnersRepository;

    public PartnerLogic(IRepository<Core.Domain.PromoCodeManagement.Partner> partnersRepository)
    {
        _partnersRepository = partnersRepository;
    }

    public async Task<IEnumerable<PartnerResponse>> GetPartnersAsync()
    {
        var partners = (await _partnersRepository.GetAllAsync())
            .ToList();
        if (!partners.Any())
            return new List<PartnerResponse>();

        var response = partners.Select(x => new PartnerResponse()
        {
            Id = x.Id,
            Name = x.Name,
            NumberIssuedPromoCodes = x.NumberIssuedPromoCodes,
            IsActive = true,
            PartnerLimits = x.PartnerLimits
                .Select(y => new PartnerPromoCodeLimitResponse()
                {
                    Id = y.Id,
                    PartnerId = y.PartnerId,
                    Limit = y.Limit,
                    CreateDate = y.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    EndDate = y.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
                    CancelDate = y.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
                }).ToList()
        });

        return response;
    }

    public async Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(
        Guid id,
        Guid limitId)
    {
        var partner = await _partnersRepository.GetByIdAsync(id);

        if (partner == null)
            throw new EntityNotFound();

        var limit = partner.PartnerLimits
            .FirstOrDefault(x => x.Id == limitId);

        if (limit == null)
            return new PartnerPromoCodeLimitResponse();

        var response = new PartnerPromoCodeLimitResponse()
        {
            Id = limit.Id,
            PartnerId = limit.PartnerId,
            Limit = limit.Limit,
            CreateDate = limit.CreateDate.ToString("dd.MM.yyyy hh:mm:ss"),
            EndDate = limit.EndDate.ToString("dd.MM.yyyy hh:mm:ss"),
            CancelDate = limit.CancelDate?.ToString("dd.MM.yyyy hh:mm:ss"),
        };

        return response;
    }

    public async Task<(Guid PartnerId, Guid NewLimitId)> SetPartnerPromoCodeLimitAsync(
        Guid id,
        SetPartnerPromoCodeLimitRequest request)
    {
        var partner = await _partnersRepository.GetByIdAsync(id);

        if (partner == null)
            throw new EntityNotFound();

        //Если партнер заблокирован, то нужно выдать исключение
        if (!partner.IsActive)
            throw new EntityBadQuery("Данный партнер не активен"); //TODO вынести текст ошибки в ресурсы

        //Проверка наличия активных лимитов
        var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
            !x.CancelDate.HasValue);

        if (activeLimit != null)
        {
            //Если партнеру выставляется лимит, то мы 
            //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
            //то количество не обнуляется
            partner.NumberIssuedPromoCodes = 0;

            //При установке лимита нужно отключить предыдущий лимит
            activeLimit.CancelDate = DateTime.Now;
        }

        if (request.Limit <= 0)
            throw new EntityBadQuery("Лимит должен быть больше 0"); //TODO вынести текст ошибки в ресурсы

        var newLimit = new PartnerPromoCodeLimit()
        {
            Limit = request.Limit,
            Partner = partner,
            PartnerId = partner.Id,
            CreateDate = DateTime.Now,
            EndDate = request.EndDate
        };

        partner.PartnerLimits.Add(newLimit);

        await _partnersRepository.UpdateAsync(partner);

        return (partner.Id, newLimit.Id);
    }
    
    public async Task CancelPartnerPromoCodeLimitAsync(Guid id)
    {
        var partner = await _partnersRepository.GetByIdAsync(id);

        if (partner == null)
            throw new EntityNotFound();

        //Если партнер заблокирован, то нужно выдать исключение
        if (!partner.IsActive)
            throw new EntityBadQuery("Данный партнер не активен"); //TODO вынести текст ошибки в ресурсы

        //Отключение лимита
        var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
            !x.CancelDate.HasValue);

        if (activeLimit != null)
        {
            activeLimit.CancelDate = DateTime.Now;
        }

        await _partnersRepository.UpdateAsync(partner);
    }
}