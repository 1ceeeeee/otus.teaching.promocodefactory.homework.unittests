namespace Otus.Teaching.PromoCodeFactory.BusinessLogic.Exceptions;

public class EntityNotFound : Exception
{
    public EntityNotFound()
    {
        
    }

    public EntityNotFound(string message) : base(message)
    {
            
    }
}