namespace Otus.Teaching.PromoCodeFactory.BusinessLogic.Exceptions;

public class EntityBadQuery : Exception
{
    public EntityBadQuery()
    {
    }

    public EntityBadQuery(string message) : base(message)
    {
    }
}